<?php
/*
 Template Name: SiteMap
*/
?>

<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

	<div class="site-map-container">
		<div class="bg-t"></div>

		<div class="wrap">
			<?= do_shortcode('[wp_sitemap_page]') ?>
		</div>
	</div>

	<? include('components/contact-us.php'); ?>

	<? include('components/case-studies.php'); ?>

<?php get_footer(); ?>
