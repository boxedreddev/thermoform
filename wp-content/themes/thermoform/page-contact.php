<?php
/*
 Template Name: Contact Page
*/
?>

<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

	<div class="contact-content">
		<div class="bg-t"></div>

		<div class="wrap">
			<div class="left-col wow fadeInUp" data-wow-duration="1s">
				<? the_field('left_column_content'); ?>
			</div>
			<div class="contact-details wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
				<p><span>Office Opening Hours:</span><? the_field('office_opening_hours'); ?></p>
				<p><span>UK Office Address:</span><? the_field('office_address'); ?></p>
				<p><span>UK Phone:</span><? the_field('telephone_number','option'); ?></p>
				<p><span>US Office Address (Head Office):</span><? the_field('office_address_us'); ?></p>
				<p><span>US Phone:</span><? the_field('telephone_number_us','option'); ?></p>
				<p><span>Email:</span><a href="mailto:<? the_field('email_address','option'); ?>"><? the_field('email_address','option'); ?></a></p>
			</div>
		</div>
	</div>

	<div class="contact-form-container">
		<div class="wrap">
			<h3 class="wow fadeInUp" data-wow-duration="1s">Get in touch with us</h3>

			<div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s"><?= do_shortcode('[contact-form-7 id="180" title="Contact Form"]'); ?></div>
		</div>
	</div>

<?php get_footer(); ?>
