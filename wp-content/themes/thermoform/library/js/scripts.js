jQuery(document).ready(function($) {

new WOW().init();

/* ====================== NAV ICON ======================  */
$('.nav-icon').click(function(){
	$('nav').toggleClass('active-nav');
	$(this).toggleClass('open');

	$('body').toggleClass('no-scroll')
	$('.hamburger-nav').toggleClass('.active-nav');
});

/* ====================== HEADER ANIMATION ======================  */
	var controller = new ScrollMagic.Controller();

	var screenHeight = $( window ).height();

	var scene = new ScrollMagic.Scene({
		triggerHook: 0, offset: 100
	})

	.setClassToggle('header', 'header-active')

	controller.addScene([
	  scene
	]);

/* ====================== PARALLAX MASTHEAD ======================  */
if ($("body").hasClass("home")) {
	const title = document.querySelector('.lower-masthead h3');
	const speed = 0.1;

	title.style.transform = 'translateY( calc( var(--scrollparallax) * -1px ) )';

	window.addEventListener('scroll', function() {
	  title.style.setProperty('--scrollparallax', (document.body.scrollTop || document.documentElement.scrollTop) * speed);
	});
}

/* ====================== Move inner MASTHEAD ======================  */
	var whiteshapescone = TweenMax.fromTo(".lower-masthead", 1, { backgroundPosition:'center top'}, { backgroundPosition:'center 80%', ease: Power0.linear}  );
	var scene = new ScrollMagic.Scene({
			triggerElement: ".upper-masthead",
			triggerHook:0.25,
			duration: 425,
			offset: 350
		})
	.setTween(whiteshapescone)
	//.addIndicators({name: "Move Other White SHAPES"})
	.addTo(controller);

/* ====================== ANIMATE PAGE BLOCK MODS ======================  */
$('.page-blocks .row').each(function(){
	var currentPageBlock = this;

	var controller = new ScrollMagic.Controller();

	var scene = new ScrollMagic.Scene({
		triggerElement: currentPageBlock, triggerHook: 0.75, duration: 1500
	})

	.setClassToggle(currentPageBlock, 'animate-page-block')

	controller.addScene([
	  scene
	]);
});

/* ====================== ANIMATE CASE STUDY MODS ======================  */
$('.case-studies-container .case-study').each(function(){
	var currentCaseStudy = this;

	var controller = new ScrollMagic.Controller();

	var scene = new ScrollMagic.Scene({
		triggerElement: currentCaseStudy, triggerHook: 0.75, duration: 1500
	})

	.setClassToggle(currentCaseStudy, 'animate-casestudy-block')

	controller.addScene([
	  scene
	]);
});


if($(window).width() >= 1200) {
	// /* ====================== BACKGROUND T MOVING ======================  */
	$('.bg-t').each(function(){
		var currentT = this;

		var controller = new ScrollMagic.Controller();

		var scene = new ScrollMagic.Scene({
			triggerElement: currentT, triggerHook: 0.75, duration: 2000
		})

		.setTween(currentT, 20, {x: 500})

		controller.addScene([
		  scene
		]);
	});
}

// /* ====================== ANIMATE TIMELINE ======================  */
$('.timeline-container .step').each(function(){
	var timelineStep = this;

	var controller = new ScrollMagic.Controller();

	var scene = new ScrollMagic.Scene({
		triggerElement: timelineStep, triggerHook: 0.5, reverse: false, offset: 40
	})

	.setClassToggle(timelineStep, 'animate-timeline')
	// .addIndicators({name: "Animate timeliner"})

	controller.addScene([
	  scene
	]);
});

/* ====================== Move inner contact bg ======================  */
	var whiteshapescone = TweenMax.fromTo(".contact-us-strip", 1, { backgroundPosition:'center top'}, { backgroundPosition:'center 80%', ease: Power0.linear}  );
	var scene = new ScrollMagic.Scene({
			triggerElement: ".contact-strip-container",
			triggerHook:0.25,
			duration: 800,
			offset: -250
		})
	.setTween(whiteshapescone)
  //.addIndicators({name: "Move contact bg"})
	.addTo(controller);


	/* ====================== NUMBERS PAGES - Move circles on scroll ======================  */
		var moveleftcircles = TweenMax.fromTo(".bg_circle_full_bg_border", 1, { }, { top:'-20%',left:'-40%', ease: Power0.linear}  );
		var scene = new ScrollMagic.Scene({
				triggerElement: ".inner-masthead-container",
				triggerHook:0.25,
				duration: 1200,
				offset: 350
			})
		.setTween(moveleftcircles)
		// .addIndicators({name: "Move some vircles1"})
		.addTo(controller);

		var moveleftcircles = TweenMax.fromTo(".bg_circle_full_border", 1, { }, { bottom:'-20%',right:'-90%', ease: Power0.linear}  );
		var scene = new ScrollMagic.Scene({
				triggerElement: ".number_wrap",
				triggerHook:0.25,
				duration: 1400,
				// offset: 350
			})
		.setTween(moveleftcircles)
		//.addIndicators({name: "Move some vircles2"})
		.addTo(controller);

		/* ====================== SPLIT BIG CIRCLE - Move circle & text on scroll ======================  */
			var moveleftcircles = TweenMax.fromTo(".large_img_left", 1, { }, { marginTop:'-50%', ease: Power0.linear}  );
			var scene = new ScrollMagic.Scene({
					triggerElement: ".wrapper_larg_circle",
					triggerHook:0.25,
					duration: 800,
					// offset: 150
				})
		 .setTween(moveleftcircles)
		 //.addIndicators({name: "Move larger Circles1"})
		 .addTo(controller);

			var moveleftcircles = TweenMax.fromTo(".bg_circle_full_border", 1, { bottom:'0' }, { bottom:'-20%',right:'-90%', ease: Power0.linear}  );
			var scene = new ScrollMagic.Scene({
					triggerElement: ".number_wrap",
					triggerHook:0.25,
					duration: 1400,
					// offset: 350
				})
			.setTween(moveleftcircles)
			// .addIndicators({name: "Move some vircles2"})
			.addTo(controller);



/* ====================== SMOOTH SCROLLING ======================  */
	$('a[href*="#"]')
	  // Remove links that don't actually link to anything
	  .not('[href="#"]')
	  .not('[href="#0"]')
	  .click(function(event) {
	    // On-page links
	    if (
	      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
	      &&
	      location.hostname == this.hostname
	    ) {
	      // Figure out element to scroll to
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	      // Does a scroll target exist?
	      if (target.length) {
	        // Only prevent default if animation is actually gonna happen
	        event.preventDefault();
	        $('html, body').animate({
	          scrollTop: target.offset().top - 60
	        }, 1000, function() {
	          // Callback after animation
	          // Must change focus!
	          var $target = $(target);
	          $target.focus();
	          if ($target.is(":focus")) { // Checking if the target was focused
	            return false;
	          } else {
	            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
	            $target.focus(); // Set focus again
	          };
	        });
	      }
	    }
	  });

	/* ====================== MASTHEAD SLIDER ======================  */
	$('.page-blocks-carousel').owlCarousel({
		loop: true,
		items: 1,
		nav: false,
		dots: true,
		navText: '',
		autoHeight: true,
		autoplay: true,
		autoplayTimeout: 5000,
		smartSpeed: 1000,
		autoplayHoverPause:true,
		mouseDrag: false,
        touchDrag: true,
		responsive: {
			768 : {
				mouseDrag: false,
        		touchDrag: true,
				nav: false,
				margin: 50,
			},
			1260 : {
				autoHeight: false,
				mouseDrag: true,
        		touchDrag: true,
				nav: true,
				margin: 100,
			}
		}
	})
});
