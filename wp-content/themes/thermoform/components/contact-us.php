<div class="contact-strip-container">
	<div class="contact-us-strip" style="background: url('<? the_field('contact_strip_background_image', 9) ?>');background-size: cover;background-repeat:no-repeat;">
		<img class="wow fadeInUp" data-wow-duration="1s" src="<? image('contact-icon') ?>.svg">

		<? if(get_field('contact_strip_text',9)): ?>
			<h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s"><? the_field('contact_strip_text', 9); ?></h3>
		<? else: ?>
			<h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Contact us today to find out more about this unique PVC-A lining system</h3>
		<? endif; ?>

		<a href="<? url('contact-us') ?>" class="btn wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">Contact Us</a>
	</div>
</div>
