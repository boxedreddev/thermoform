<div class="hp-masthead">
	<div class="upper-masthead">
		<div class="masthead-logo wow fadeInUp" data-wow-duration="1s"></div>
		<h1 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s"><? the_field('masthead_text'); ?></h1>

		<a class="down-indicator wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s" href="#lower-masthead-trigger">
			<span></span><span></span>
		</a>
	</div>
	<div class="lower-masthead">
		<div class="wrap">
			<div id="lower-masthead-trigger"></div>
			<h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s"><? the_field('lower_masthead_text', $post->ID ); ?></h3>
		</div>
	</div>
</div>
