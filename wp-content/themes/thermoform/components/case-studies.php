<div class="case-studies-strip">
	<div class="bg-t"></div>
	<h2 class="wow fadeInUp" data-wow-duration="1s">Case Studies</h2>

	<div class="case-studies-container">
		<? get_the_case_studies() ?>
	</div>
</div>
