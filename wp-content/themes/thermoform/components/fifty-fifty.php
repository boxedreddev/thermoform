<div class="fifty-fifty">
	<div class="bg-t"></div>
	<div class="wrap">
		<? while ( have_rows('fifty_fifty_section', $post->ID) ) : the_row(); ?>
			<div class="row">
				<div class="content wow fadeInUp" data-wow-duration="1s">
					<? if(get_sub_field('row_title', $post->ID)): ?>
						<h3><? the_sub_field('row_title', $post->ID); ?></h3>
					<? endif ; ?>
					<? the_sub_field('fifty_fifty_section_content', $post->ID); ?>

					<? if(get_sub_field('row_btn', $post->ID)): ?>
						<a class="btn lime" href="<? the_sub_field('row_btn', $post->ID); ?>">
							<?php if(get_sub_field('row_btn_text',$post->ID)) {
								echo the_sub_field('row_btn_text', $post->ID);
							}
							else {
								echo 'Find Out More';
							}
							?>
						</a>
					<? endif ; ?>

				</div>
				<div class="row-image wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" style="background: url('<? the_sub_field('fifty_fifty_section_image', $post->ID); ?>') center / cover no-repeat;"></div>
			</div>
		<? endwhile; ?>
	</div>
</div>
