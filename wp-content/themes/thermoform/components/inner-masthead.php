<div class="inner-masthead-container">
	<div class="inner-masthead">
		<? if(is_home()): ?>
			<h1 class="wow fadeInUp" data-wow-duration="1s">News</h1>
		<? else: ?>
			<h1 class="wow fadeInUp" data-wow-duration="1s"><? the_field('masthead_title', $post->ID) ?></h1>
			<p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s"><? the_field('masthead_snippet', $post->ID) ?></p>
		<? endif; ?>
	</div>
</div>
