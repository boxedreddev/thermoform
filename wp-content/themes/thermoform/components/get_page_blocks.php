<div class="page-blocks-container inner-page-blocks">

  <?php

  if ( is_page_template( array( 'page-thermoform-lining-system.php', 'page-alt-page.php', 'page-numbers.php' ) ) ) {
     echo '<div class="wrapper_topname">';
  }
  ?>

  <div class="page-blocks-header">
    <h2><? the_field('page_blocks_title', 9); ?></h2>
  </div>

  <div class="page-blocks-carousel owl-carousel">
    <? while ( have_rows('page_blocks', 9) ) : the_row();

         $post_objects = get_sub_field_object('page_link',9);

         if ($post_objects['value'] == get_the_permalink($post->ID)) {

         }
         else {
         ?>
          <div class="row">
                <div class="row-content">
                  <h3><? the_sub_field('page_block_title', 9); ?></h3>
                  <p><? the_sub_field('page_block_content', 9); ?></p>
                  <a class="btn" href="<? the_sub_field('page_link', 9); ?>">CLICK HERE</a>
              </div>
              <?php if(get_sub_field('optional_side_image')) {
                ?>
                <div class="row-image" style="background: url('<? the_sub_field('optional_side_image', 9); ?>') center/ cover no-repeat;"></div>
                <?php
              }
              else {
                ?>
                <div class="row-image" style="background: url('<? the_sub_field('page_block_image', 9); ?>') center/ cover no-repeat;"></div>
                <?php
              }
              ?>
          </div>
      <?
      }
    endwhile; ?>
    </div>
    <?php
    if ( is_page_template( array( 'page-thermoform-lining-system.php', 'page-alt-page.php', 'page-numbers.php' ) ) ) {
        echo '</div>';
    }
    ?>
</div>
