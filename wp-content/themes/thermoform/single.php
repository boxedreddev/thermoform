<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

		<?php $thumb_id = get_post_thumbnail_id($post->ID);
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$thumb_url = $thumb_url_array[0]; ?>

	<div class="single-post-container">
		<div class="bg-t"></div>

		<div class=" wrap">
			<div class="featured-image wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" style="background: url('<?= $thumb_url; ?>') center / cover no-repeat;"></div>
				<? the_content(); ?>
		</div>

	</div>

	<? include('components/contact-us.php'); ?>

	<? include('components/case-studies.php'); ?>

<?php get_footer(); ?>
