<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

	<div class="news-overview wrap">

		<? $i = 0; ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php $thumb_id = get_post_thumbnail_id($post);
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
			$thumb_url = $thumb_url_array[0]; ?>

			<article class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?= $i; ?>s">
				<div class="article-content">
	    	    	<h3><? the_title(); ?></h3>
	    	    	<p class="date"><?php the_time('jS M Y') ?></p>
	    	    	<? $post_content = get_the_content() ?>
					<p><?php echo wp_trim_words($post_content, 30, '...')?></p>
	    	    	<a class="btn" href="<?= get_the_permalink($post->ID) ?>">CLICK HERE</a>
	    	    </div>
	    		<div class="article-image" style="background: url('<?= $thumb_url; ?>') top center / cover;"></div>
			</article>

		<? $i += 0.5; ?>
		<?php endwhile; ?>

		<?php bones_page_navi(); ?>
	</div>

<?php get_footer(); ?>
