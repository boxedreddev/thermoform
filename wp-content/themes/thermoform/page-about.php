<?php
/*
 Template Name: About Page
*/
?>

<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

	<? include('components/fifty-fifty.php'); ?>

	<? include('components/contact-us.php'); ?>

	<div class="circle-modules">
		<div class="bg-t"></div>

		<div class="wrap">

			<? $i = 0; ?>
			<? while ( have_rows('circle_modules') ) : the_row(); ?>
				<div class="mod wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?= $i; ?>s">
	    			<div class="circle-image" style="background: url('<? the_sub_field('circle_image'); ?>') center/ cover no-repeat;"></div>
					<h3><? the_sub_field('module_title'); ?></h3>
    		    	<p><? the_sub_field('module_content'); ?></p>
    		    	<a class="btn lime" href="<? the_sub_field('find_out_more_button_destination'); ?>">FIND OUT MORE</a>
				</div>

				<? $i += 0.5; ?>
			<? endwhile; ?>

		</div>
	</div>

<?php get_footer(); ?>
