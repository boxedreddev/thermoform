<?php
/*
 Template Name: Alternate Sub Page
*/
?>

<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

	<div class="alternate_content">

		<div class="wrap">
			<div class="content_cntn">
				<? $gifimagger = get_field('gif_image'); ?>
				<img src="<? echo $gifimagger['url']; ?>" class="gif_image_lrg wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.125s">

				<?php if(get_field('optional_intro_title')) {
					echo '<h2>'.get_field('optional_intro_title').'</h2>';
				}
				?>



				<div class="wow fadeInUp" data-wow-duration="1s">
					<? the_field('introduction_textarea'); ?>

					<?php if(get_field('optional_button') || get_field('optional_link')) {
						echo '<a href="'.get_field('optional_link').'" class="btn lime">'.get_field('optional_button').'</a>';
					}
					?>

				</div>
			</div>

		</div>

		<div class="wrap wrapper_larg_circle">
			<? $gifleftside = get_field('left_circle_image'); ?>
			<img src="<? echo $gifleftside['url']; ?>" class="large_img_left">

			<div class="right_text_wrap">


				<div class="para_wrap wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.15s">
					<?php if(get_field('optional_right_title')) {
						echo '<h3>'.get_field('optional_right_title').'</h3>';
					}
					?>

					<div>
						<? the_field('right_paragraph_text'); ?>

						<?php if(get_field('optional_button_text') && get_field('optional_button_link')) {
							echo '<a href="'.get_field('optional_button_link').'" class="btn lime">'.get_field('optional_button_text').'</a>';
						}
						?>

					</div>

				</div>

			</div>
		</div>
	</div>

	<? //include('components/fifty-fifty.php'); ?>

	<? include('components/get_page_blocks.php'); ?>

	<? include('components/contact-us.php'); ?>

	<? include('components/case-studies.php'); ?>

<?php get_footer(); ?>
