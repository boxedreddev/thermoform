			<footer>
					<a class="footer-logo"></a>

					<nav>
						<?php wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Footer Links', 'bonestheme' ),
						'menu_class' => 'footer-nav',
						'theme_location' => 'footer-links',
						)); ?>
					</nav>

					<div class="contact-details">
						<p>E: <a href="mailto:<? the_field('email_address', 'option') ?>"><? the_field('email_address', 'option') ?></a></p>
						<p>UK: <a href="tel:<? the_field('telephone_number', 'option') ?>"><? the_field('telephone_number', 'option') ?></a></p>
						<p>US: <a href="tel:<? the_field('telephone_number_us', 'option') ?>"><? the_field('telephone_number_us', 'option') ?></a></p>
					</div>

					<div class="lower-strip">
						<p class="source-org copyright">&copy; Copyright <?php bloginfo( 'name' ); ?> <?php echo date('Y'); ?> | <a href="<? url('privacy-cookies-policy') ?>">Privacy & Cookies Policy</a> | <a href="<? url('sitemap'); ?>">Sitemap</a> | <a href="<? url('terms-and-conditions'); ?>">T & Cs</a></p>
						<p>Owned by Warrior USA</p>
					</div>

			</footer>

		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
