<?php
/*
 Template Name: Homepage
*/
?>

<?php get_header(); ?>

	<? include('components/hp-masthead.php'); ?>

	<div class="timeline-container">
		<div class="bg-t"></div>
		<div class="timeline">
			<h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">Why is Thermoform Unique?</h2>

			<? while ( have_rows('timeline') ) : the_row(); ?>
				<div class="step">
				<div class="step-content">
		        	<h5><? the_sub_field('timeline_number'); ?></h5>
		        	<p><? the_sub_field('timeline_content'); ?></p>
		        </div>
		        <svg viewBox="0 0 167 324">
					<g id="Layer_2" data-name="Layer 2">
						<g id="Layer_1-2" data-name="Layer 1">
							<path class="cls-1" d="M28.5,17.5a11,11,0,1,1-11-11A11,11,0,0,1,28.5,17.5Zm-11,278a11,11,0,1,0,11,11A11,11,0,0,0,17.5,295.5Zm11,10.47A144.51,144.51,0,0,0,28.49,18"/>
						</g>
					</g>
				</svg>

	        	<div class="step-icon" style="background: url('<? the_sub_field('timeline_icon'); ?>') center/ contain no-repeat;"></div>
	        	</div>
	         <? endwhile; ?>
        </div>

         <a class="btn" href="<? echo get_field('button_link'); ?>"><? the_field('button_text'); ?></a>
	</div>

	<div class="page-blocks-container">
		<div class="bg-t"></div>

		<div class="page-blocks-header">
			<h2 class="wow fadeInUp" data-wow-duration="1s"><? the_field('page_blocks_title'); ?></h2>
		</div>

		<div class="page-blocks">
			<? while ( have_rows('page_blocks') ) : the_row(); ?>
				<div class="row">
					<div class="row-content">
		    	    	<h3><? the_sub_field('page_block_title'); ?></h3>
		    	    	<p><? the_sub_field('page_block_content'); ?></p>
		    	    	<a class="btn" href="<? the_sub_field('page_link'); ?>"><? echo the_sub_field('button_text_pger'); ?></a>
		    	    </div>

							<div class="row-image" style="background: url('<? the_sub_field('page_block_image'); ?>') center/ cover no-repeat;"></div>

	    		</div>
	    	<? endwhile; ?>
    	</div>
	</div>

	<? include('components/contact-us.php'); ?>

	<? include('components/case-studies.php'); ?>

<?php get_footer(); ?>
