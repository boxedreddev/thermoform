<?php
/*
 Template Name: Sub Page
*/
?>

<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

	<? include('components/fifty-fifty.php'); ?>

	<? include('components/get_page_blocks.php'); ?>

	<? include('components/contact-us.php'); ?>

	<? include('components/case-studies.php'); ?>

<?php get_footer(); ?>
