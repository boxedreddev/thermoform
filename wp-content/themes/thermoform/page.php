<?php get_header(); ?>

<? include('components/inner-masthead.php'); ?>

<div class="fallback-wrap">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


		<?php
			// the content (pretty self explanatory huh)
			the_content();
		?>

<?php
endwhile;
endif; ?>

</div>

<?php get_footer(); ?>
