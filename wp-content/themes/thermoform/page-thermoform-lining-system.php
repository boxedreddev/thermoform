<?php
/*
 Template Name: Thermoform Lining System
*/
?>

<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

	<div class="lower-masthead-container">
		<div class="thermoform-lower-masthead">
			<div class="wrap">
				<?php if(get_field('top_icon_title')) {
					echo '<h2>'. get_field('top_icon_title') .'</h2>';
				}
				?>
			</div>
			<div class="wrap">
				<? $i = 0; ?>
				<? while ( have_rows('top_icons') ) : the_row(); ?>
					<div class="icon-container wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?= $i; ?>s">
						<div class="icon" style="background: url('<? the_sub_field('top_icons_icon'); ?>') center / contain no-repeat;"></div>
						<h4><? the_sub_field('title'); ?></h4>
					</div>

					<? $i += 0.5; ?>
				<? endwhile; ?>
			</div>
		</div>
	</div>

	<?// include('components/fifty-fifty.php'); ?>

	<div class="intro_section">
		<div class="wrap">

			<?php if(get_field('intro_opt_title')) {
				echo '<h3>'.get_field('intro_opt_title').'</h3>';
			}
			?>

			<div class="top_para_wrap">
				<? the_field('top_paragraph_content'); ?>
			</div>

		</div>

		<div class="wrap wrapper_larg_circle">

			<? $gifleftside = get_field('left_circle_image'); ?>
			<img src="<? echo $gifleftside['url']; ?>" class="large_img_left">

			<div class="right_text_wrap">
				<!-- <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.125s">Fold and Form Pipe</h3> -->
				<div class="para_wrap wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.15s">
					<? the_field('right_paragraph_text'); ?>
				</div>
				<a class="btn lime wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s" href="<?php the_field('page_to_link_to'); ?>" style="visibility: visible; animation-duration: 1s; animation-delay: 1s; animation-name: fadeInUp;">Find Out More</a>
			</div>

		</div>
	</div>

	<div class="green_tt_bg">
		<div class="diameter-section">
			<!-- <div class="circle-container">
				<div class="circles wow fadeInRightBig" data-wow-duration="2s"></div>
			</div> -->

			<div class="wrap">
				<div class="diameter-content">
					<h2 class="wow fadeInUp" data-wow-duration="1s"><? the_field('diameter_title'); ?></h2>
					<div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".25s"><? the_field('diameter_content'); ?></div>
					<a class="btn lime wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" href="<? the_field('find_out_more_button_destination'); ?>"><?php the_field('button_text');?></a>

					<div class="stats">
						<h3 class="wow fadeInUp" data-wow-delay=".75s" data-wow-duration="1s">Available in</h3>
						<img class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".75s" alt="Diameter Statistic" src="<? image('diameter-stat-new.png') ?>" style="height:auto !important;">
					</div>
				</div>

				<div class="diameter_gif wow slideInRight" data-wow-duration="1s" data-wow-offset="100">
						<img src="<?php echo the_field('diameter_gif');?>">
				</div>
			</div>
		</div>
	</div>

	<? include('components/get_page_blocks.php'); ?>

	<? include('components/contact-us.php'); ?>

	<? include('components/case-studies.php'); ?>

<?php get_footer(); ?>
