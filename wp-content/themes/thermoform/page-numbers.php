<?php
/*
 Template Name: Numbered Content Area
*/
?>

<?php get_header(); ?>

	<? include('components/inner-masthead.php'); ?>

	<div class="numbers_content">

			<div class="wrap">

				<div class="content_cntn">

					<?
					$gifimagger = get_field('gif_image');
					$videolinker = get_field('video_yt_url');
					if($gifimagger) {
					?>

						<img src="<? echo $gifimagger['url']; ?>" class="gif_image_lrg">

					<?php }
					elseif ($videolinker) {
						?>
						<iframe src="<?php echo $videolinker; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						<?php
					}?>

					<? if( get_field('steps_title' )) { ?>
						<h2><? the_field('steps_title'); ?></h2>
					<?php } ?>

				</div>

				<div class="number_wrap">

					<? if (get_field('step_one')) { ?>
					<div class="number_entry">
						<img src="<?php echo image('numbers/1.png');?>" class="number">
						<p><? the_field('step_one'); ?></p>
					</div>
					<?php } ?>

					<? if (get_field('step_two')) { ?>
					<div class="number_entry">
						<img src="<?php echo image('numbers/2.png');?>" class="number">
						<p><? the_field('step_two'); ?></p>
					</div>
					<?php } ?>

					<? if (get_field('step_three')) { ?>
					<div class="number_entry">
						<img src="<?php echo image('numbers/3.png');?>" class="number">
						<p><? the_field('step_three'); ?></p>
					</div>
					<?php } ?>

					<? if (get_field('step_four')) { ?>
					<div class="number_entry">
						<img src="<?php echo image('numbers/4.png');?>" class="number">
						<p><? the_field('step_four'); ?></p>
					</div>
					<?php } ?>



				</div>

			</div>

		<div class="wrap wrapper_larg_circle">
			<? $gifleftside = get_field('left_circle_image'); ?>
			<img src="<? echo $gifleftside['url']; ?>" class="large_img_left">

			<div class="right_text_wrap">


				<div class="para_wrap wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.15s">
					<?php if(get_field('optional_right_title')) {
						echo '<h3>'.get_field('optional_right_title').'</h3>';
					}
					?>

					<div>
						<? the_field('right_paragraph_text'); ?>

						<?php if(get_field('optional_button_text') && get_field('optional_button_link')) {
							echo '<a href="'.get_field('optional_button_link').'" class="btn lime">'.get_field('optional_button_text').'</a>';
						}
						?>

					</div>

				</div>

			</div>
		</div>

		<img src="<?php echo image('full_circle_with_bg.png');?>" class="bg_circle_full_bg_border">
		<img src="<?php echo image('circle_bg.png');?>" class="bg_circle_full_border">


	</div>

	<? //include('components/fifty-fifty.php'); ?>

	<? include('components/get_page_blocks.php'); ?>

	<? include('components/contact-us.php'); ?>

	<? include('components/case-studies.php'); ?>

<?php get_footer(); ?>
