<?php

# =========== CASE STUDIES STRIP =========== #
function get_the_case_studies() {
	$args = array(
	'post_status'   	=> 'publish',
	'post_type'        	=> 'case-studies',
	'posts_per_page'	=> 2,
	);

	$query = new WP_Query($args);

	$i = 0;

	foreach($query->posts as $post): ?>
		<?php $thumb_id = get_post_thumbnail_id($post);
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$thumb_url = $thumb_url_array[0]; ?>

		<div class="case-study wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?= $i; ?>s">
			<div class="case-study-image-container"><div class="case-study-image" style="background: url('<? echo $thumb_url; ?>') center / cover;"></div></div>

			<div class="case-study-snippet">
				<p class="date"><? the_time('jS F Y') ?></p>
				<h3><?= get_the_title($post->ID); ?></h3>
				<p><? the_field('case_study_snippet', $post->ID) ?></p>
				<a class="btn" href="<?= get_the_permalink($post->ID); ?>">Read More</a>
			</div>
		</div>

	<? $i += 0.5; ?>
	<? endforeach;
}

# =========== ALLOW OPTIONS =========== #
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Site Settings',
	));
}

# =========== MOVE YOAST TO BOTTOM =========== #
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
